%++++++++++++++++++++++++++++++++++++++++
% Don't modify this section unless you know what you're doing!
\documentclass[letterpaper,11pt]{article}
\usepackage{natbib}
\bibliographystyle{unsrtnat}
\usepackage{tabularx} % extra features for tabular environment
\usepackage{amsmath}  % improve math presentation
\usepackage{graphicx} % takes care of graphic including machinery
\usepackage[acronym]{glossaries}
\usepackage[margin=1in,letterpaper]{geometry} % decreases margins
\usepackage{booktabs}
%\usepackage{cite} % takes care of citations
\usepackage[final]{hyperref} % adds hyper links inside the generated pdf file
\hypersetup{
	colorlinks=true,       % false: boxed links; true: colored links
	linkcolor=blue,        % color of internal links
	citecolor=blue,        % color of links to bibliography
	filecolor=magenta,     % color of file links
	urlcolor=blue         
}
%+++++++++++++++++++++++++++++++++++++++
\begin{document}

\title{NI-MVI: semestral project \\\textbf{Time series modelling}}
\author{Jan Perina\\perinja2@fit.cvut.cz}
\date{\today}
\maketitle

\begin{abstract}
In this semestral project I explored the possibilities os using the neural networks for time series modelling. Several architectures of neural networks were used on two data-sets.
\end{abstract}
\thispagestyle{empty}

\pagebreak
\section{Introduction}
Time series are sequences of observations over time. These observations represent a measured process, such as stock market prices, the amount of carbon dioxide in atmosphere, or population growth. We can measure a lot of statistical properties of the time series, e. g. seasonality, trend, auto-correlation, or stationarity. These properties can be then used to estimate parameters for statistical models, such as \acrfull{ar}, \acrfull{ma} or \acrfull{arima}. However, lots of statistical models assume normality, or linearity, which often is not present in the process.

The main goal of using the neural networks on time series data is to overcome these shortcomings by utilizing the ability of the neural network to learn relations hidden in data. In this project, I will experiment with experiment with \acrfull{cnn}, \acrfull{rnn}, \acrfull{lstm}, and \acrfull{gru}. The time series themselves can be modelled either as one single process (univariete), or several processes at once (multivariate). 


\section{Datasets}
For the purposes of this project, I've decided to use two data-sets. The first being the statistics about the Covid-19 disease in Czech Republic, and the second being the time series of Apple Inc. stock.  The Apple stock dataset contains data spanning from January 2006 to October 2017 with increasing trend (Figure~\ref{fig:apple}).

\begin{figure}[!h]
    \centering
    \includegraphics[width=\columnwidth,keepaspectratio]{images/apple.png}
    \caption{Apple stock over time}
    \label{fig:apple}
\end{figure}

The Covid-19 dataset contains daily statistics about newly infected, deceased, recovered, and the number of tests performed. In both cumulative and incremental form. I decided to focus on the incremental ones. The time span is from February 2020, until December 2021. The data contains four major peaks. From the chart in Figure~\ref{fig:covid}, It is visible that daily new cases, newly deceased and recovered act very similarly, however, the number of tests performed developed differently over time.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{images/covid.png}
    \caption{Covid-19 dataset (log. scale)}
    \label{fig:covid}
\end{figure}
\pagebreak
\section{Data Preprocessing}
Both data-sets didn't need any cleaning, or processing on data level, but to improve the predictions I used a MinMaxScaler from the scikit-learn library. Since the stock data had daily minimum and maximum, I created daily average from them. On top of that, to be able to model the data, the shape of the data had to be changed. The prediction in this project is made as window-to-window prediction, which takes an input window of fixed size (e. g. last four days) and prediction window of fixed window (e. g. next 2 days). For the Covid-19 dataset, I took first 52 weeks for training and the following 10 weeks as a validation set.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.3\columnwidth]{images/window.png}
    \caption{Data transformation example (3:1)}
    \label{fig:data}
\end{figure}

\section{Model architectures}
For the sake of fair comparison of the the models, I've tried to create as similar architectures as possible.  
The \acrshort{cnn} consisted of a one 1D convolution layer with a sliding window of a \textit{window\_size} parameter and a ReLu function to eliminate noise. The next one was \acrshort{rnn}, which consist of 2 recurrent layers, then \acrshort{lstm} model with 2 layers of \acrshort{lstm} cells, and the final one being \acrshort{gru} model with two layers as well. In addition, to be able to model the time series, I added one fully connected layer taking ten input features and outputting one value. 
\begin{figure}[!h]
\centering
\begin{verbatim}
CNNModel(
  (conv): Conv1d(1, 10, kernel_size=(window_size,), stride=(1,))
  (relu): ReLU(inplace=True)
  (fc): Linear(in_features=10, out_features=1, bias=True)
)
\end{verbatim}
    \caption{Architecture CNN}
    \label{fig:cnn}
\end{figure}

\begin{figure}[!h]
\centering
\begin{verbatim}
RNNModel(
  (rnn): RNN(1, 10, num_layers=2, batch_first=True, dropout=0.1)
  (fc): Linear(in_features=10, out_features=1, bias=True)
)
\end{verbatim}
    \caption{Architecture RNN}
    \label{fig:rnna}
\end{figure}

 \begin{figure}[!h]
\centering
\begin{verbatim}
LSTMModel(
  (lstm): LSTM(1, 10, num_layers=2, batch_first=True)
  (fc): Linear(in_features=10, out_features=1, bias=True)
)
\end{verbatim}
    \caption{Architecture LSTM}
    \label{fig:lstma}
\end{figure}

 \begin{figure}[!h]
\centering
\begin{verbatim}
GRUModel(
  (gru): GRU(1, 10, num_layers=2, batch_first=True)
  (fc): Linear(in_features=10, out_features=1, bias=True)
)
\end{verbatim}
    \caption{Architecture GRU}
    \label{fig:grua}
\end{figure}

\section{Experiments \& Results}
Initially, I tried to find some reasonable training setup, which was conditioned by measuring the error of prediction beforehand. For this purpose, I used the \acrfull{mae}, together with \acrfull{adam} with weight decay. The learning rate was set to 0.001 and weight decay to 0.01. The number of training epochs are shown in Table~\ref{tab:epochs}.

\begin{table}[!h]
\centering
\begin{tabular}{|l|r|} 
\toprule
Model & num. epochs  \\ 
\hline
CNN   & 2000 \\ 
\hline
RNN   & 8000 \\ 
\hline
LSTM  & 2100 \\ 
\hline
GRU   & 2000 \\
\bottomrule
\end{tabular}
    \caption{Training epochs}
    \label{tab:epochs}
\end{table}

However, I still was not sure about the size of input window, therefore I tried various window sizes, from 2 to 7. The results are shown in Figure~\ref{fig:size_error}, on the left side with prediction \acrshort{mae} and on the right side \acrshort{mae} when both prediction and original were transformed into weekly sliding window. Values in the first plot are, except maybe for \acrshort{lstm}, overall decreasing when the window size increases. On the other hand, in the 7-day average plot, the \acrshort{mae} increases with sliding window, however the errors are way smaller compared to the ones in left plot. Visualization of the results are shown in Figure~\ref{fig:covid_pred}, and in Figure~\ref{fig:covid_weekly} for 7-day average. After this analysis, I ended up using \textit{window\_size} of 7 and trained the models once again.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\columnwidth]{images/covid_error.png}
    \caption{Error of prediction on different input window size}
    \label{fig:size_error}
\end{figure}

\begin{figure}[!h]
    \centering
    \includegraphics[width=\columnwidth]{images/covid_pred.png}
    \caption{Caption}
    \label{fig:covid_pred}
\end{figure}


\begin{figure}[!h]
    \centering
    \includegraphics[width=\columnwidth]{images/unicovid_weekly.png}
    \caption{Caption}
    \label{fig:covid_weekly}
\end{figure}


In both charts above, we can see that majority of models, except the \acrshort{cnn} which started giving inaccurate predictions at the end of the time series, performed well, with some smaller deviations.

\begin{table}[!h]
    \centering
  \begin{tabular}{llrr}
\toprule
network &          MAE \\
\midrule
cnn &   966.423508 \\
     rnn &   580.175647 \\
    lstm &  1145.743485 \\
     gru &   875.929796 \\
\bottomrule
\end{tabular}
    \caption{MAE on Covid dataset}
    \label{tab:maecov}
\end{table}

\subsection{Apple Stocks}
Based on the experiment with Covid dataset, I reused the model architectures, but the training configuration had to be updated based on the train and validation loss. The model predictions shown in Figure~\ref{fig:applepred} shows, that worst results in term of \acrshort{mae} came from \acrshort{lstm}, it covers the long term increasing trend, but is unable to capture the dynamic changes in data, probably due to its high reliance in memory, since changing the amount of training epochs did not improve the results much. \acrshort{cnn} tends to predict the data well, but after January 2016 it predicts almost constant values. The \acrshort{gru} and \acrshort{rnn} had very similar predictions, but there was still a large gap when the stock price started growing. The results are shown in Table~\ref{tab:apple_mae}. 

\begin{figure}[!h]
    \centering
    \includegraphics[width=\columnwidth]{images/apple_pred.png}
    \caption{Apple stock prediction}
    \label{fig:applepred}
\end{figure}

\begin{table}[!h]
    \centering
  \begin{tabular}{lrr}
\toprule
 network & num. epochs & MAE   \\
\midrule
cnn & 1000 &  10.742588 \\
rnn & 1000  &  8.013728 \\
lstm & 8000 &  15.089151 \\
gru & 750  &  7.351920 \\
\bottomrule
\end{tabular}

    \caption{Apple Stock experiment }
    \label{tab:apple_mae}
\end{table}


\subsection{Multivariate modelling of Covid data}
As mentioned above, the Covid-19 dataset contains multiple time series. I initially tried to model all the incremental columns from the dataset, but I often received unsatisfactory prediction for the time series relating to number of tests performed. I assumed this is due to the different behavior of these processes over time, I decided to skip them and model only the rest of the columns.


In the process of training, I had to tune the training parameters to improve the prediction, the final configuration is shown in Table~\ref{tab:mulicov_mae}. The model with best \acrshort{mae} on the time series was \acrshort{cnn} with \acrshort{mae} of 492.421996. The overall form of modelled time series shown in Figure~\ref{fig:cnnpredm}
\begin{table}[!h]
    \centering
  \begin{tabular}{lrrrr}
\toprule
 network & num. epochs & learning rate & weight decay & MAE   \\
\midrule
cnn & 9500 & 0.001 &  0.01 &  492.421996 \\
rnn & 12000  & 0.002 &  0.009  &   527.019289 \\
lstm & 1850 & 0.003 & 0.01 &   679.705582 \\
gru & 2200  & 0.001 & 0.015&  695.267479\\
\bottomrule
\end{tabular}

    \caption{Apple Stock experiment }
    \label{tab:mulicov_mae}
\end{table}

\begin{figure}[!h]
    \centering    
    \includegraphics[width=\columnwidth]{images/multicov.png}
    \includegraphics[width=\columnwidth]{images/multicovlog.png}
    \includegraphics[width=\columnwidth]{images/covid.png}

    \caption{Predictions of the most accurate model (CNN)}
    \label{fig:cnnpredm}
\end{figure}


\pagebreak
\section{Conclusion}
In this project, I was able to try modeling the time series with various deep neural networks in PyTorch, which was quite challenging from the beginning, however, I think I was able to produce some fairly reasonable results. However, the architectures used were probably no way near optimal, but this was to compare the architectures more fairly. I think that modeling time series is rather challenging task, since compared to standard prediction tasks, there is the unpredictable factor of time, which makes the task harder. In the case of large time-series, like the Apple stock market one, it would also be beneficial for the sake of the performance to retrain the model when it stops perform well.


\newacronym{ar}{AR}{Autoregressive model}
\newacronym{ma}{MA}{Moving average}
\newacronym{arima}{ARIMA}{Autoregresive Integrated Moving average model}
\newacronym{cnn}{CNN}{Convolutional Neural Network}
\newacronym{rnn}{RNN}{Recurrent Neural Network}
\newacronym{lstm}{LSTM}{Long Short-term Memory}
\newacronym{gru}{GRU}{Gated Recurrent Unit}
\newacronym{mae}{MAE}{Mean Absolute Error}
\newacronym{adam}{ADAM}{ADAptive Momentum estimation}
% \clearpage

% \printglossary
% \appendix

% \section*{Appendix}



\end{document}
